package com.ks.bancario.accounts.debits;

import com.ks.bancario.accounts.Operations;
import sun.misc.Perf;

/**
 * Created by migue on 29/11/2016.
 */
public class Perfiles extends Debit implements Operations
{
    public Perfiles()
    {
        balance = 0;
        bankFees = 2;
    }

    public boolean addBalance(float amount)
    {
        balance += amount;
        return true;
    }

    public boolean subStract(float amount)
    {
        if (balance - amount < 0)
        {
            return false;
        }
        else
        {
            balance -= amount;
            return true;
        }
    }
}
