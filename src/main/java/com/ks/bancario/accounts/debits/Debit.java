package com.ks.bancario.accounts.debits;

import com.ks.bancario.accounts.Account;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by migue on 16/11/2016.
 */
public class Debit extends Account
{
    private static final Logger VMobjLog = LogManager.getLogger(Debit.class);

    private static int contador;

    protected int bankFees;

    static
    {
        contador = 0;
    }

    protected Debit()
    {

        contador += 1;
        if (VMobjLog.isDebugEnabled())
        {
            VMobjLog.debug("Se han creado " + contador + " cuentas de debito");
        }
    }

    public static int getContador()
    {
        return contador;
    }

    public int getBankFees()
    {
        return bankFees;
    }

    public void setBankFees(int bankFees)
    {
        this.bankFees = bankFees;
    }
}
