package com.ks.bancario.accounts;


import org.apache.logging.log4j.*;
import org.apache.logging.log4j.Logger;

import java.util.Date;

/**
 * Created by migue on 16/11/2016.
 */
public abstract class Account
{
    private static final Logger VMobjLog = LogManager.getLogger(Account.class);

    private static int contador;
    private String card;
    private String clabe;
    protected float balance;
    private Date expiration;
    private String cvv;
    protected float annuity;

    static
    {
        contador = 0;
    }

    protected Account()
    {
        contador += 1;
        if (VMobjLog.isInfoEnabled())
        {
            VMobjLog.info("Se han creado " + contador + " cuentas generales");
        }
        balance = 0;
        card = "";
        clabe = "";
        annuity = (float) 500.00;
    }

    public String getCard()
    {
        return card;
    }

    public void setCard(String card)
    {
        this.card = card;
    }

    public String getClabe()
    {
        return clabe;
    }

    public void setClabe(String clabe)
    {
        this.clabe = clabe;
    }

    public float getBalance()
    {
        return balance;
    }

    public Date getExpiration()
    {
        return expiration;
    }

    public void setExpiration(Date expiration)
    {
        this.expiration = expiration;
    }

    public String getCvv()
    {
        return cvv;
    }

    public void setCvv(String cvv)
    {
        this.cvv = cvv;
    }

    public float getAnnuity()
    {
        return annuity;
    }

    public static int getContador()
    {
        return contador;
    }
}