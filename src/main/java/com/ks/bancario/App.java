package com.ks.bancario;

import com.ks.bancario.accounts.Account;
import com.ks.bancario.accounts.Operations;
import com.ks.bancario.accounts.credits.Classic;
import com.ks.bancario.accounts.credits.Credit;
import com.ks.bancario.accounts.debits.Debit;
import com.ks.bancario.accounts.debits.Perfiles;
import com.ks.bancario.databases.SQLlite;
import com.ks.bancario.databases.Save;
import com.ks.bancario.databases.Txt;
import com.ks.bancario.person.Client;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Hello world!
 */
public class App
{
    private static final Logger VMobjLog = LogManager.getLogger(App.class);

    public static void main(String[] args)
    {
        if (args[0].toString().equals("1"))
        {
            Txt VLobjDatos = new Txt();
            VLobjDatos.setBase("mibase.txt");
            Save.setConnection(VLobjDatos);
        }
        else
        {
            Save.setConnection(new SQLlite());
        }
        Client VLobjCliente = new Client();
        VLobjCliente.setName("Miguel Hernandez");

        Save VLobjGaurdar = new Save();
        VLobjGaurdar.saveClient(VLobjCliente);
    }
}
