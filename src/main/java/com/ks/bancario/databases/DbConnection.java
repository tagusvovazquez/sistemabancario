package com.ks.bancario.databases;

/**
 * Created by migue on 08/12/2016.
 */
public interface DbConnection
{
    public boolean connect();
    public boolean insert(String query);
    public boolean upadte(String query);
    public boolean isConnected();
}
